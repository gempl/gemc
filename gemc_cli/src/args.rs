use clap::ArgAction;
use std::path::{PathBuf};
use clap::builder::{Styles};
use clap_derive::{Parser};
use crate::log::LogLevel;

pub use clap::Parser;
use colored::Colorize;

#[derive(Parser, Debug)]
#[command(version, about="Simple Gem Compiler made with Rust + LLVM.", styles=Styles::plain())]
pub struct Args {
    /// The input file path
    #[arg(id="INPUT_FILE")]
    input: PathBuf,

    /// The output file path
    #[arg(short='o', id="OUTPUT_FILE")]
    output: Option<PathBuf>,

    /// Cache directories
    #[arg(short='I', id="DIR", help="The include directory where cached symbols should be stored", default_value="~/.cache/gemc")]
    cache_dirs: Vec<PathBuf>,

    /// If set no color output should be produced
    #[arg(long="no-color", help="Colorless output", action=ArgAction::SetFalse)]
    color: bool,

    /// Set true if gem compiler should be generating cache
    #[arg(long="no-cache", help="Don't cache the the symbols", default_value="true", action=ArgAction::SetFalse)]
    should_cache: bool,

    #[arg(long="no-build", help="Don't build but generates cache and lints", default_value="true", action=ArgAction::SetFalse)]
    no_build: bool,

    /// Log level the user desires, should be used for logs
    #[arg(short, long="log", help="Log level [quiet, minimal, normal, verbose, debug]", default_value="normal", value_parser=parse_log_level)]
    logger: LogLevel
}

/// Parse the log level
fn parse_log_level(s: &str) -> Result<LogLevel, String> {
    match s {
        "quiet" => Ok(LogLevel::Quiet),
        "minimal" => Ok(LogLevel::Minimal),
        "normal" => Ok(LogLevel::Normal),
        "verbose" => Ok(LogLevel::Verbose),
        "debug" => Ok(LogLevel::Debug),
        "05/12/24" => Err(format!("Unknown log level `{}`! But it's sure is an awesome date <3", s)),
        _ => Err(format!("Unknown log level `{}`!", s))
    }
}

impl Args {
    /// Get the input file path
    pub fn get_input(&self) -> PathBuf {
        self.input.clone()
    }

    /// Get the output file path
    ///
    /// OR
    ///
    /// Generate an output file path out of input file, if none provided
    pub fn get_output(&self) -> PathBuf {
        self.output.clone().unwrap_or_else(|| {
            let input = self.get_input();
            let filename = input.file_name().unwrap().to_owned();
            let mut output = PathBuf::from(filename);
            output.set_extension("o");
            output
        })
    }

    /// Should the compiler output colors
    pub fn is_colorized(&self) -> bool {
        self.color
    }

    /// Give the directories where cache should be stored, or empty if not generating cache
    pub fn cache_dirs(&self) -> Vec<PathBuf> {
        if self.is_generating_cache() {
            self.cache_dirs.clone()
        }
        else {
            vec![]
        }
    }

    /// Should the compiler generate cache
    pub fn is_generating_cache(&self) -> bool {
        self.should_cache
    }

    /// Get the logger level, SHOULD be used to output logs
    pub fn get_logger(&self) -> &LogLevel {
        &self.logger
    }
}