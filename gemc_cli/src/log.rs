use clap_derive::ValueEnum;

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, ValueEnum)]
pub enum LogLevel {
    /// Nothing is printed
    Quiet,
    /// Minimal output, no warnings or info, only errors
    Minimal,
    /// Normal output, warnings + errors, some info, no debug information
    Normal,
    /// Verbose output, errors + warnings + info
    Verbose,
    /// Debug output
    Debug
}

/// Set if colors should be applied to text
pub fn set_colorized(color: bool) {
    colored::control::set_override(color);
}

#[macro_export]
macro_rules! log {
    ($log:expr, minimal: $min:expr, normal: $norm:expr, verbose: $verb:expr, debug: $debug:expr) => {
        log_minimal!($log, $min);
        log_normal!($log, $norm);
        log_verbose!($log, $verb);
        log_debug!($log, $debug);
    };

    ($log:expr, normal: $norm:expr, verbose: $verb:expr, debug: $debug:expr) => {
        log_normal!($log, $norm);
        log_verbose!($log, $verb);
        log_debug!($log, $debug);
    };

    ($log:expr, verbose: $verb:expr, debug: $debug:expr) => {
        log_verbose!($log, $verb);
        log_debug!($log, $debug);
    };
}

pub use colored::Colorize;

#[macro_export]
macro_rules! log_error {
    ($msg:expr) => {
        print!("{} {}", "ERROR:".bright_red().bold(), $msg);
    };

    ($ty:expr, $msg:expr) => {
        println!("{} {}{} {}", "ERROR:".bright_red().bold(), Colorize::bold(String::from($ty).as_str()).white(), ":".white().bold(), $msg);
    };
}

#[macro_export]
macro_rules! log_minimal {
    ($log:expr, $msg:expr) => {
        match $log {
            gemc_cli::log::LogLevel::Minimal |
            gemc_cli::log::LogLevel::Normal |
            gemc_cli::log::LogLevel::Verbose |
            gemc_cli::log::LogLevel::Debug => print!("{}", $msg),
            _ => {}
        }
    };
}

#[macro_export]
macro_rules! log_normal {
    ($log:expr, $msg:expr) => {
        match $log {
            gemc_cli::log::LogLevel::Normal |
            gemc_cli::log::LogLevel::Verbose |
            gemc_cli::log::LogLevel::Debug => print!("{}", $msg),
            _ => {}
        }
    };
}

#[macro_export]
macro_rules! log_verbose {
    ($log:expr, $msg:expr) => {
        match $log {
            gemc_cli::log::LogLevel::Verbose |
            gemc_cli::log::LogLevel::Debug => print!("{}", $msg),
            _ => {}
        }
    };
}

#[macro_export]
macro_rules! log_debug {
    ($log:expr, $msg:expr) => {
        match $log {
            gemc_cli::log::LogLevel::Debug => print!("{}", $msg),
            _ => {}
        }
    };
}