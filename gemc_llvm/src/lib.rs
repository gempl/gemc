use std::ffi::c_uint;
use llvm_sys_181::core::LLVMGetVersion;

pub fn llvm_get_version() -> (u32, u32, u32) {
    unsafe {
        let mut major: c_uint = 0;
        let mut minor: c_uint = 0;
        let mut patch: c_uint = 0;
        LLVMGetVersion(&mut major as _, &mut minor as _, &mut patch as _);
        (major as u32, minor as u32, patch as u32)
    }
}