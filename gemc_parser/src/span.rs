
/// Stores raw span of a token or symbol in a string
pub struct Span {
    lo: usize,
    hi: usize,
}