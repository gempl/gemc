use std::process::Output;
use crate::lexer::token_stream::TokenStream;
use crate::span::Span;

pub struct ParseError {
    msg: String,
    lines: Vec<String>,
    span: Span,
    /// TODO: implement tip system
    tip: Option<()>
}

type ParseResult<T> = Result<ParseError, T>;

trait Parse {
    fn parse(&self, ts: &TokenStream) -> ParseResult<Self> where Self: Sized;
}