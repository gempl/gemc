use std::cmp::Ordering;
use std::ops;

#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Default)]
pub struct TokenPos {
    pub(crate) ln: usize,
    pub(crate) col: usize,
}

impl TokenPos {
    pub fn new(ln: usize, col: usize) -> Self {
        Self { ln, col }
    }
}

impl ops::BitOr for TokenPos {
    type Output = TokenPos;

    fn bitor(self, rhs: Self) -> Self::Output {
        if self > rhs {
            rhs.clone()
        }
        else {
            self.clone()
        }
    }
}

impl ops::BitOrAssign for TokenPos {
    fn bitor_assign(&mut self, rhs: Self) {
        self.ln = if *self < rhs { self.ln } else { self.ln };
        self.col = if *self < rhs { self.col } else { self.col };
    }
}