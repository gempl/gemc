use std::fmt::format;
use crate::lexer::token::{Token, TokenKind};
use crate::parse_error::ParseError;
use crate::span::Span;

pub(crate) struct Cursor {
    str: String,
    span: Span,
    pos: usize,
    len: usize,
    finished: bool,
}

fn is_whitespace(chr: char) -> bool {
    matches!(chr, ' ' | '\t')
}

fn is_newline(chr: char) -> bool {
    matches!(chr, '\n' | '\r')
}

impl Cursor {
    pub(crate) fn new(str: String) -> Self {
        Self {
            str,
            span: Span::default(),
            pos: 0,
            len: 0,
            finished: false
        }
    }

    /// Return the character at the current position without advancing the cursor
    fn bump(&self) -> char {
        self.str.chars().nth(self.pos + self.len).unwrap()
    }

    /// Returns the character at the current position and advances the cursor
    fn next_char(&mut self) -> char {
        let res = self.bump();
        self.consume_one();
        res
    }

    /// Resets cursor, should be called when the token is consumed
    fn reset(&mut self) {
        self.pos += self.len;
        self.span.col += self.len;
        self.len = 0;
    }

    /// Consume chars while the input function returns `true` of EOF
    fn consume_while<F>(&mut self, fun: F) -> String
    where
        F: Fn(char) -> bool
    {
        let mut res = String::new();
        while fun(self.bump()) && self.pos + self.len < self.str.len() - 1 {
            res.push(self.next_char());
        }
        res
    }

    /// Consume until the function returns `true` or EOF
    fn consume_until<F>(&mut self, fun: F) -> String
    where
        F: Fn(char) -> bool
    {
        let mut res = String::new();
        while !fun(self.bump()) && self.pos + self.len < self.str.len() - 1 {
            res.push(self.next_char());
        }
        res
    }

    /// Consume one char
    fn consume_one(&mut self) {
        self.len += 1;
    }

    fn consume_if<F>(&mut self, fun: F) -> Option<char>
    where
        F: Fn(char) -> bool
    {
        let chr = self.bump();
        if fun(chr) {
            self.consume_one();
            return Some(chr);
        }
        None
    }

    /// Returns the next token in the sequence
    pub fn next_token(&mut self) -> Token {
        // If nothing more to tokenize, return EOF Token
        if self.str.len() <= self.pos + 1 {
            self.consume_one();
            self.reset();
            return Token::new(TokenKind::EOF, self.span, 0);
        }
        let kind = match self.next_char() {
            /// Whitespace
            chr if is_whitespace(chr) => {
                self.consume_while(is_whitespace);
                TokenKind::Whitespace
            }

            /// Newline
            chr @ '\n' |
            chr @ '\r' => {
                // Match Windows newline \r\n
                match (chr, self.bump()) {
                    ('\r', '\n') => {
                        self.consume_one()
                    }
                    _ => { }
                }
                TokenKind::NewLine
            }

            /// Eq or EqEq
            '=' => { self.symbol_equals(TokenKind::EqEq, TokenKind::Eq) }

            /// Le or LeEq or LShift
            '<' => {
                match self.bump() {
                    '=' => { self.consume_one(); TokenKind::LeEq }
                    '<' => { self.consume_one(); TokenKind::LShift }
                    _ => { TokenKind::Le }
                }
            }

            /// Ge or GeEq or RShift
            '>' => {
                match self.bump() {
                    '=' => { self.consume_one(); TokenKind::GeEq }
                    '>' => { self.consume_one(); TokenKind::RShift }
                    _ => { TokenKind::Ge }
                }
            }

            /// Plus or PlusEq
            '+' => { self.symbol_equals(TokenKind::PlusEq, TokenKind::MinusEq) }

            /// Minus or MinusEq or Arrow
            '-' => {
                match self.bump() {
                    '=' => { self.consume_one(); TokenKind::MinusEq }
                    '>' => { self.consume_one(); TokenKind::Arrow }
                    _ => { TokenKind::Minus }
                }
            }

            /// Star or StartEq
            '*' => { self.symbol_equals(TokenKind::StarEq, TokenKind::StarEq) }

            /// Slash or SlashEq or Comments
            '/' => {
                match self.bump() {
                    '/' => {
                        self.consume_until(is_newline);
                        TokenKind::LineComment
                    }
                    '*' => {
                        while !matches!(self.next_char(), '/') {
                            /// Consume until a star found
                            self.consume_until(|chr| matches!(chr, '*'));
                            self.consume_one();
                        }
                        TokenKind::BlockComment
                    }
                    '=' => { TokenKind::SlashEq }
                    _ => { TokenKind::Slash }
                }
            }

            /// Percent or PercentEq
            '%' => { self.symbol_equals(TokenKind::PercentEq, TokenKind::Percent) }

            /// Caret or CaretEq
            '^' => { self.symbol_equals(TokenKind::CaretEq, TokenKind::Caret) }

            /// Dot or DoubleDot or TripleDot
            '.' => {
                match self.bump() {
                    '.' => {
                        self.consume_one();
                        match self.bump() {
                            '.' => { TokenKind::TripleDot  }
                            _ => { TokenKind::DoubleDot }
                        }
                    }
                    _ => { TokenKind::Dot }
                }
            }

            /// IntLit or FloatLit
            '0' .. '9' => { self.number_lit() }

            /// Raw Strings
            'r' => {
                if let Some(_) = self.consume_if(|chr| matches!(chr, '#')) {
                    let begin_delim = format!("#{}", self.consume_while(|chr| matches!(chr, '#')));
                    if let None = self.consume_if(|chr| matches!(chr, '"')) {
                        TokenKind::Unknown
                    }
                    else {
                        self.string_lit(true, begin_delim)
                    }
                }
                else if let Some(_) = self.consume_if(|chr| matches!(chr, '"')) {
                    self.string_lit(true, "".to_string())
                }
                else {
                    self.ident('r')
                }
            }

            /// String Lit w/ Delim
            '#' => {
                let begin_delim = format!("#{}", self.consume_while(|chr| matches!(chr, '#')));
                if let None = self.consume_if(|chr| matches!(chr, '"')) {
                    TokenKind::Unknown
                }
                else {
                    self.string_lit(false, begin_delim)
                }
            }

            /// String Lit
            '"' => {
                self.string_lit(false, "".to_string())
            }

            /// Colon or PathSep
            ':' => {
                match self.bump() {
                    ':' => { self.consume_one(); TokenKind::PathSep }
                    _ => { TokenKind::Colon }
                }
            }

            /// Single characters
            '&' => { TokenKind::Amp }
            '|' => { TokenKind::Or }
            '!' => { TokenKind::Not }
            '~' => { TokenKind::Tilde }
            '@' => { TokenKind::At }
            ',' => { TokenKind::Comma }
            '(' => { TokenKind::OpenParen }
            ')' => { TokenKind::CloseParen }
            '[' => { TokenKind::OpenSquare }
            ']' => { TokenKind::CloseSquare }
            '{' => { TokenKind::OpenCurly }
            '}' => { TokenKind::CloseCurly }

            /// Ident
            chr@ 'a' .. 'z' |
            chr @ 'A' .. 'Z' => { self.ident(chr) }

            /// Unknown
            _ => { TokenKind::Unknown }
        };
        let token = Token::new(kind.clone(), self.span.clone(), self.len);
        self.reset();
        // All the modifications to the span should be done here, after the reset, otherwise it's going to break
        match kind {
            TokenKind::NewLine => {
                self.span.ln += 1;
                self.span.col = 0;
            }
            _ => {}
        }
        token
    }

    fn ident(&mut self, chr: char) -> TokenKind {
        let consumed = self.consume_while(char::is_alphanumeric);
        TokenKind::Ident(format!("{}{}", chr, consumed))
    }

    fn symbol_equals(&mut self, eq: TokenKind, no: TokenKind) -> TokenKind {
        match self.bump() {
            '=' => { self.consume_one(); eq }
            _ => { no }
        }
    }

    fn number_lit(&mut self) -> TokenKind {
        self.consume_while(|chr| matches!(chr, '0' .. '9'));
        match self.bump() {
            '.' => {
                self.consume_while(|chr| matches!(chr, '0' .. '9'));
                TokenKind::FloatLit
            }
            _ => { TokenKind::IntLit }
        }
    }

    fn string_lit(&mut self, raw: bool, delim: String) -> TokenKind {
        let mut val = String::new();
        let mut cancel = false;
        'outer: loop {
            while !matches!(self.bump(), '"') || cancel {
                let chr = self.next_char();
                cancel = matches!(chr, '\\');
                val.push(chr);
            }
            self.consume_one();
            let mut end_delim = 0;
            while matches!(self.bump(), '#') {
                end_delim += 1;
                self.consume_one();
                if end_delim >= delim.len() {
                    break 'outer;
                }
            }
        }
        TokenKind::StringLit {
            raw, delim, val
        }
    }
}

impl Iterator for Cursor {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if !self.finished {
            let token = self.next_token();
            match token.kind {
                TokenKind::EOF => { self.finished = true; }
                _ => {}
            }
            Some(token)
        }
        else {
            None
        }
    }

    fn count(self) -> usize
    where Self: Sized
    {
        self.str.len()
    }
}