
use crate::lexer::token::Token;
use crate::span::Span;

/// Constant structure that the Lexer returns when lexical analysis is finished successfully
/// Used by the Parser to build AST
pub struct TokenStream {
    origin: String,
    cur: usize,
    tokens: Vec<Token>,
    span_stack: Vec<Span>
}

impl TokenStream {
    pub(crate) fn new(origin: String, tokens: Vec<Token>) -> Self {
        Self {
            origin,
            cur: 0usize,
            tokens,
            span_stack: Vec::new()
        }
    }

    pub fn current(&self) -> &Token {
        self.tokens.get(self.cur).unwrap()
    }


}