use std::fmt;
use std::fmt::Formatter;
use crate::span::Span;


#[derive(Debug, Clone, Hash, Ord, PartialOrd, Eq, PartialEq, Default)]
pub enum TokenKind {
    /// \n\r or \n or \r
    NewLine,
    /// ' ' or '\t'
    Whitespace,

    /// //
    LineComment,
    /// /* */
    BlockComment,

    /// =
    Eq,
    /// ==
    EqEq,
    /// <
    Le,
    /// <=
    LeEq,
    /// >
    Ge,
    /// >=
    GeEq,

    /// &
    Amp,
    /// |
    Or,
    /// !
    Not,
    /// ~
    Tilde,
    /// <<
    LShift,
    /// >>
    RShift,

    /// +
    Plus,
    /// -
    Minus,
    /// *
    Star,
    /// /
    Slash,
    /// %
    Percent,
    /// ^
    Caret,

    /// +=
    PlusEq,
    /// -=
    MinusEq,
    /// *=
    StarEq,
    /// /=
    SlashEq,
    /// %=
    PercentEq,
    /// ^=
    CaretEq,

    /// @
    At,
    /// .
    Dot,
    /// ..
    DoubleDot,
    /// ...
    TripleDot,
    /// ,
    Comma,
    /// :
    Colon,
    /// ::
    PathSep,
    /// ->
    Arrow,
    /// _
    Underscore,

    /// (
    OpenParen,
    /// )
    CloseParen,
    /// [
    OpenSquare,
    /// ]
    CloseSquare,
    /// {
    OpenCurly,
    /// }
    CloseCurly,

    /// Integer literal
    IntLit,
    /// Float literal
    FloatLit,
    /// Strings
    StringLit {
        raw: bool,
        delim: String,
        val: String,
    },

    /// Idents, at this point keywords are idents too
    Ident(String),

    /// Unknown token, impossible to determine
    #[default]
    Unknown,

    /// End Of File
    EOF
}

#[derive(Debug, Clone, Hash, Ord, PartialOrd, Eq, PartialEq, Default)]
pub struct Token {
    pub(crate) kind: TokenKind,
    pub(crate) span: Span,
    pub(crate) len: usize,
}

impl Token {
    pub(crate) fn new(kind: TokenKind, span: Span, len: usize) -> Self {
        Self { span, kind, len }
    }
}