use std::str::Chars;
use crate::lexer::cursor::Cursor;
use crate::lexer::token::{Token, TokenKind};
use crate::lexer::token_stream::TokenStream;
use crate::parse_error::ParseError;
use crate::span::Span;

pub fn tokenize(code: &str) -> TokenStream {
    let mut tokens: Vec<Token> = vec![];

    let mut cur = Cursor::new(code.to_string());
    for token in cur {
        match token.kind {
            /// Skip whitespaces and comments from getting put into the compilation process
            TokenKind::LineComment |
            TokenKind::BlockComment |
            TokenKind::Whitespace => { continue; }
            /// Combine new line symbols for easier parsing with the previous one if exist
            TokenKind::NewLine => {
                if let Some(last) = tokens.last_mut() && matches!(last.kind, TokenKind::NewLine) {
                    last.len += token.len;
                    continue;
                }
            }
            _ => {}
        }
        tokens.push(token);
    }

    TokenStream::new(code.to_string(), tokens)
}