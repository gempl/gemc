use crate::span::Span;

#[derive(Debug, Clone)]
pub enum TipKind {
    /// Remove a part of the code.
    /// eg:
    /// ```example
    /// ERROR: Semi-colon is redundant
    /// hello: String;
    ///              -
    /// hello: String
    /// ```
    Rm,
    /// Add to the code at the span.
    /// eg:
    /// ```example
    /// ERROR: Colon is required after variable declaration
    /// hello String
    /// hello: String
    ///      +
    /// ```
    Add { val: String },
    /// Substitute a part of the code.
    /// eg:
    /// ```example
    /// ERROR: Expected `->`, found `:`
    /// func some(): Result<Null> { ... }
    ///            -
    /// func some() -> Result<Null> { ... }
    ///            +++
    /// ```
    Sub { val: String }
}

#[derive(Debug, Clone)]
#[derive()]
pub struct ErrorTip {
    kind: TipKind,
    msg: String,
}

#[derive(Debug, Clone, Default)]
pub struct ParseError {
    pub(crate) msg: String,
    pub(crate) span: Span,
    pub(crate) tip: Option<ErrorTip>
}