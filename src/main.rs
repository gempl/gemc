use std::fs;
use std::path::PathBuf;
use std::process::exit;
use gemc_cli::args::{Args, Parser};
use gemc_cli::{log_debug, log_error, log_minimal, log_normal, log_verbose};
use gemc_cli::log::{set_colorized, Colorize};
use gemc_llvm::llvm_get_version;
use gemc_parser::lexer::tokenize;

/// Package version, should be used in the header
const VERSION: &str = env!("CARGO_PKG_VERSION");

/// Read the input file to a string,
/// or show an error if the function can't find the file
fn read_input_file(path: PathBuf) -> String {
    fs::read_to_string(&path).unwrap_or_else(|err| {
        log_error!(format!("Unable to open a file `{}`: {}", path.display(), err));
        exit(-1);
    })
}

/// Shows the compiler header. Used for debugging and bug reports.
///
/// Should be in format from the lowest log level to highest.
/// Should output nothing in minimal or normal.
fn show_compiler_header(args: &Args) {
    let log = args.get_logger();
    log_normal!(log, format!("gemc version: {}\n", VERSION));
    log_verbose!(log, format!("input file: {}\n", args.get_input().display()));
    log_verbose!(log, format!("output file: {}\n", args.get_output().display()));
    log_debug!(log, "==== DEBUG INFO ====\n");
    log_debug!(log, {
        let version = llvm_get_version();
        let ver_str = format!("{}.{}.{}", version.0, version.1, version.2);
        format!("llvm version: {}\n", ver_str)
    });
}

fn main() {
    // Parse Arguments
    let args = Args::parse();
    let log = args.get_logger();

    set_colorized(args.is_colorized());

    // Show the compiler header
    show_compiler_header(&args);

    // Read the file
    log_verbose!(log, "Reading the file... ".bright_white().bold());
    let input_path = args.get_input();
    let code = read_input_file(input_path);
    log_verbose!(log, "DONE\n".green().bold());

    // Start compiling...
    let timer = std::time::Instant::now();
    log_verbose!(log, "Starting compilation process...\n".bright_white().bold());

    // Start Parsing...
    log_verbose!(log, "\tParsing... ".bright_white().bold());

    // Tokenize
    let tokens = tokenize(code.as_str());

    log_verbose!(log, "DONE\n".green().bold());

    log_verbose!(log, "FINISHED!\n".bright_white().bold());
}