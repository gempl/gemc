# What is GemPL
Gem is a statically typed, JIT & AOT compiled programming language. It was made to be a fast, safe, useful, and easy general purpose language, inspired by Rust, Go, and Zig.
#### this DESIGN.md file is not intended to be used as a wiki, this is an internal piece of documentation describing the functions and design quirks of the language.

# Types
| Name | Description | Example of a Definition |
| ---- | ----------- | ------- |
| `mut T` | Mutable value of T | `mut I64` |
| (`const`) `T` | Constant value of T | `const I64` |
| `Null` | Nothing, used for returns or pointers | `Null` |
| `I8/I16/I32/I64` | Signed Integer | `I64` |
| `U8/U16/U32/U64` | Unsigned Integer | `U64` |
| `F32/F64` | Float point values | `F64` |
| `Bool` | Boolean value | `Bool` |
| `&T` | Reference to a value of T | `&mut I32` |
| `*T` | Pointer to a value of T (unsafe) | `*mut I32` |
| `[T, S]` | Sized Array of T | `[U8, 4]` |
| `[T, _]` | Unsized Array of T (unsafe), cannot be replicated. Added for compitability reasons | `[I32, _]` |
| `func(A...) -> T` | Function type | `func(I32, I32) -> I32` |
| `(T)` | Tuple | `(I32, I32)` |
| `( name: T )` | Named Tuple | `( num1: I32, num2: I32 )`

# Mutability
Every value in Gem is inherently immutable unless otherwise stated with `mut` keyword. Mutable value (for eg. return value of a function) can be made Immutable, but not the other way around. Example:
```gem
hello: mut String = get_hello_world_string() // ERROR: func() -> const String
hello: String = get_hello_world_string() // fine: func() -> const String
instance: const SomeType = get_instance() // fine: func() -> mut SomeType
instance: mut SomeType = get_instance() // fine: func() -> mut SomeType
```
If mutability is not stated, the compiler should not try to guess it, but state it as `const`.

# Runtime & Compile-Time Execution
Function marked `comptime`, will be evaluated during the compilation (may increase the compile time by a lot! and should be able turned off). Example:
```gem
comptime func[S: U32, T] sum(...args: [T, S]) -> T {
    res: mut T = 0
    for val in args {
        res += val
    }
    return res
}

val = sum(0, 15, 64, 24, 64, 36, 32)
println(val) // 235, but sum is never called during runtime, increasing speed during complex const computations
```
Arguments of the `comptime` function should be constant literals and results of other `comptime` functions.

Output Value of the `comptime` function/block cannot include any mutable elements, references, or pointers.

`comptime` block can be used to execute code at compile time:
```gem
func[S: U32, T] sum(...args: [T, S]) -> T {
    res: mut T = 0
    for val in args {
        res += val
    }
    return res
}

runtime_sum = sum(0, 1, 2, 3) // sum is called at runtime
comptime_sum = comptime { sum(0, 1, 2, 3) |> } // sum is called at compile time
```

# Syntax
Syntax of Gem is heavily inpired by Rust and Go.
## Variables
Either type or value is required because compiler doesn't guess Types after the variable definition.
```gem 
<name>: <Type> (= <value>)
<name> = <value>
```

## Functions
Types are always required in function definitions, unless it's `Null` return type.
```gem
func <name>(<arguments>) -> <return type> {
    ...
}
```

## Types
```gem
type <name> = <Type Definition>
```

## Tuples instead of structures/types
Any type can implement a function, so tuples are used instead of Rust equivalent structures. Example:
```gem
type Person = (
    pub name: String,
    pub age: I32
)

impl Person /* not the same as impl (String, I32), but the tuple can be cast to Person */ {
    func new(name: String, age: I32) -> Self {
        Self (
            name, age
        ) |>
    }
}

mike = Person::new("Mike", 32)
println(mike.name)

sam = ("Sam", 45) as Person
println(sam.name)
```

## Inheritance
Gem uses two strategies for inheritance: Extending types and Traits. Both of them work to provide a powerful way of extending and creating types.
```gem
type Post = (
    pub message: String,
)

type Person = (
    pub name: String,
    pub age: I32,
    pub uid: I64,
    pub posts: Vector[Post]
)

trait EditPower {
    func editMessage(self, post: Post, message: String) 
        where Self extends Person {
        post.message = message
    }
}

type Admin extends Person { }/* Only one super is allowed, but multiple traits are allowed */
impl Admin with EditPower { }

mike = Person(
    name = "Mike",
    age = 32,
    uid = 101,
    posts = Vector::new([ Post( message: "Hello, World!" ) ])
)

// mike.editMessage(...) // ERROR: Person doesn't have "editMessage"

lara = Admin(
    name = "Lara",
    age = 25,
    uid = 3945,
    posts = Vector::new()
)
lara.editMessage(mike.posts[0], "This is not allowed!")
```

## Generics
Generics types that are generated at the place of use. Generics can be applied to:
```gem
func[T] name(...) { }
type[T] name { }
trait[T] name { }
```
In `impl` block, every generic trait that isn't filled should have the same name as the declaration and every trait generic should be satisfied:
```gem
type[T] SomeType = () 
impl SomeType[T] with Into[String] { ... }
```
### Constant Generics
Constant generics are values in generics:
```gem
func[V: T] name(...) { }
type[V: T] name { }
trait[V: T] name { }
```
It can be used for variadics:
```gem
func[S: U32] sum(...argv: [U32, S]) -> U32 { ... }
```
Or to create types of constant size:
```gem
type[S: U32] SomeType = (
    some_field: [U32, S]
)
```

## Enums, Unions, and Enum Union
#### Enums 
Enums are simple numbers with names. Example:
```gem
type IPType = enum {
    V4 = 4, 
    V6 = 6
}

// Usage
IPType::V6
```

#### Union Types
```gem
type Type1 = (CString, CString)
type Type2 = (String, String)
type Union = union { Type1, Type2, ([U64, 2]) }
```
The compiler will add an appropriate size parameter at the beginning of the Union, to determined (`match`ed) the type at runtime
```gem
match union_value as {
    Type1 -> { /* union_value is now Type1 */ }
    Type2 -> { /* union_value is now Type2 */ }
    ([U64, 2]) -> { /* union_value is now ([U64, 2]) */ }
}
```

#### Enum Unions
```gem
type IP = union {
    V4: (U8, U8, U8, U8),
    V6: (U16, U16, U16, U16)
}
```
Same as regular unions, but the type can now be `match`ed by name:
```gem
match union_value as {
    V4 -> { /* unions_value is now IP::V4 */ }
    V6 -> { /* unions_value is now IP::V6 */ }
}
```

# Externs & C Compatibility
Functions can be `extern`ed to give a declaration vs definition
```gem
extern func hello() -> Null
```
Programmer can use `extern "C"` to not mangle the name of the declared function. Extern-C function are inherently `unsafe`
```gem
extern "C" func hello() -> Null
```

# Unsafe code
`unsafe` keyword is used to mark functions as unsafe. Any function that can produce undefined behavior or any kind of fault (eg. segfault), should be marked unsafe.
```gem
unsafe func cause_segfault() {
    &(nil as *mut I32) = 101 // Will cause segfault
}
```
`unsafe` block should be used when calling unsafe functions or using unsafe operations. Programmer takes the responsibility of using `unsafe` block, and should be cover all the posibilities of undefined behavior or fault.
```gem
ptr = get_pointer_from_c()
if ptr == nil {
    panic()
}
unsafe { (ptr as &SomeType).someField = 10 }
```